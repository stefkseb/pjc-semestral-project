#ifndef MAIN_INCLUDED
#define MAIN_INCLUDED

#include <chrono>

struct Settings_s
{
    Settings_s() = default;
    int K = 2;
    bool threaded = false;
};

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp)
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

#endif