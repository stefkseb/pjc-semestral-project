#include "point.hpp"

#include <algorithm>

Point::Point(float x, float y) : x(x), y(y) {}

Mean::Mean(float x, float y, int id) : Point(x ,y), own_id(id) {}

std::ostream& operator<<(std::ostream& os, const Point& p)
{
    os << "x = " << p.x << ", y = " << p.y << "\n";
    return os;
}

float Point::getX()
{
    return this->x;
}
float Point::getY()
{
    return this->y;
}

void Mean::addIndex(int idx)
{
    this->member_indices.push_back(idx);
}

bool Mean::checkEmpty()
{
    return this->member_indices.empty();
}

void Mean::updateMean(std::vector<std::unique_ptr<Point>>& pts)
{
    long long sumaX = 0;
    long long sumaY = 0;
    int size = this->member_indices.size();
    for(auto& i : this->member_indices)
    {
        sumaX += pts[i].get()->getX();
        sumaY += pts[i].get()->getY();
    }
    this->x = sumaX / size;
    this->y = sumaY / size;
}

void Mean::vectorAssign()
{
    this->member_indices_prev.clear();
    this->member_indices_prev = this->member_indices;
    this->member_indices.clear();
}

bool Mean::vectorChanged()
{
    std::sort(this->member_indices.begin(), this->member_indices.end());
    if(this->member_indices.size() == this->member_indices_prev.size())
    {
        if(std::equal(this->member_indices.begin(), this->member_indices.end(), this->member_indices_prev.begin()))
            return false;
    }
    return true;
}

void Mean::printPoints(std::vector<std::unique_ptr<Point>>& pts, int c, std::ofstream& file)
{
    for(auto& i : this->member_indices)
    {
        file << pts[i].get()->getX() << " " << pts[i].get()->getY() << " " << c << "\n";
    }
}

void Mean::concatVectors(std::vector<int> v2)
{
    this->member_indices.insert(this->member_indices.end(), v2.begin(), v2.end());
}