#ifndef POINT_INCLUDED
#define POINT_INCLUDED

#include <memory>
#include <ostream>
#include <vector>
#include <fstream>
#include <mutex>

class Point
{
   protected:
    float x;
    float y;

   public:
    Point(float x, float y);
    ~Point() = default;

    float getX();
    float getY();

    friend std::ostream& operator<<(std::ostream& os, const Point& p);
};

class Mean : public Point
{
   private:
    std::vector<int> member_indices;
    std::vector<int> member_indices_prev;

   public:
    Mean(float , float , int);
    ~Mean() = default;
    int own_id;

    void addIndex(int);
    bool checkEmpty();
    void updateMean(std::vector<std::unique_ptr<Point>>& pts);
    void vectorAssign();
    bool vectorChanged();
    void printPoints(std::vector<std::unique_ptr<Point>>& pts, int c, std::ofstream& file);
    void concatVectors(std::vector<int>);
};

#endif