#include "handle_data.hpp"

#include "main.hpp"

int loadPoints(PointsArray_t& points)
{
    float x, y;
    int points_loaded = 0;
    while(std::cin >> x >> y)
    {
        points.push_back(std::make_unique<Point>(x, y));
        points_loaded++;
    }

    return points_loaded;
}

int saveToFile(MeansArray_t& means, PointsArray_t& points)
{
    std::ofstream file;
    file.open("output.txt", std::ios::out);
    if(!file.is_open())
    {
        return 1;
    }

    for(size_t j = 0; j < means.size(); j++)
    {
        means[j].get()->printPoints(points, j, file);
    }
    file.close();

    return 0;
}