#ifndef KMEANS_INCLUDED
#define KMEANS_INCLUDED

#include <iostream>
#include <memory>
#include <vector>

#include "main.hpp"
#include "point.hpp"

using PointsArray_t = std::vector<std::unique_ptr<Point>>;
using MeansArray_t = std::vector<std::unique_ptr<Mean>>;

int kMeans(const Settings_s&, PointsArray_t&, MeansArray_t&);
void initialize(int, PointsArray_t&, MeansArray_t&);
double get_random_double(int, int);
void findNearestMean(Point*, MeansArray_t&, int);
void findNearestMeanParallel(Point* , MeansArray_t& , int , int , int);
#endif