#ifndef DATALOAD_INCLUDED
#define DATALOAD_INCLUDED

#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

#include "point.hpp"

using PointsArray_t = std::vector<std::unique_ptr<Point>>;
using MeansArray_t = std::vector<std::unique_ptr<Mean>>;

int loadPoints(PointsArray_t& points);
int saveToFile(MeansArray_t& means, PointsArray_t& points);
#endif