# Shlukování bodů algoritmem K-means

## Popis zadání

Problém kategorizace neznámých dat do K tříd ve dvoudimenzionálním prostoru aneb shlukování bodů v rovině, a to metodou K-means s náhodnou inicializací.

## Popis algoritmu K-means a implementace

Existuje mnoho různých algoritmů, které se dají na problém klastrování dat použít. Námi zvolený algoritmus K-means bere na vstupu počet tříd, do kterých má rozklasifikovat data. Přitom minimalizuje součet čtverců vzdáleností bodů v jednom klastru. 
><img src="https://latex.codecogs.com/gif.latex?\sum_{i=0}^{n}\min_{\mu_j&space;\in&space;C}(||x_i&space;-&space;\mu_j||^2)" title="\sum_{i=0}^{n}\min_{\mu_j \in C}(||x_i - \mu_j||^2)" />


Předpokladem pro data je aby byla konvexní a izotropní. To neznamená, že by data, která nesplňují dané pořadavky, algoritmus nedokázal rozklasifikovat, ale výsledek nebude pravděpodobně smysluplný. 

Inicializace středů K tříd probíhá náhodně tak, že se vybere K různých bodů z datové sady. Algroitmus garantuje, že v konečném počtu kroků dokonverguje do lokálního minima. **Důsledkem toho a náhodné inicializace mohou dávat dva běhy algoritmu nad stejnými daty různé výsledky.** Stejně tak se liší i počet iterací, které jsou třeba na konvergenci k minimu. *(Více v diskusi k měření)*

### Kroky algoritmu
1. Inicializace středů, vybere se náhodně K různých pozic bodů z množiny dat
2. Klasifikace všech dat, každý bod do jednoho klastru, k jemuž středu má nejmenší kvadrát euklidovské vzdálenosti
3. Posunutí středu klastru do aritmetického průměru všech bodů v klastru, je li nějaký klastr prázdný, jdeme na bod 1 (reinicializace)
4. Pokud se ve dvou po sobě jdoucích iteracích množiny bodů, určující, do kterých klastrů body patří, nezměnily, nebo minimum nebylo nalezeno v limitu počtu iterací,  zastavíme algoritmus

### Popis paralelního běhu a synchonizace
Každé vlákno má při jedné iteraci algoritmu, kdy se zjišťuje ke každému bodu nejbližší střed, (*počet dat / počet vláken*) bodů, na kterých má počítat. Aby se běh vláken nemusel synchronizovat mutexy při přístupu do společného vektoru všech středů, je každému vláknu vytvořeno lokální pole, kam ukládá výsledky výpočtu, které se po doběhnutí všech vláken spojí v hlavním vlákně. Tato technika se ukázala efektivnější.

## Jak pracovat s programem

Program načítá body ze **standardního vstupu**, na kterých provádí klasifikaci. Program přijímá přepínače
> -k [num] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Specifikace počtu klastrů, do kterých chceme klasifikovat, kde *num* je číslo od 2 do 20.
>
> -t &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Program využije paralelního běhu na více vláknech
>
> --help &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Vypíše použití programu a skončí

Výstup programu je soubor **output.txt**, který je v následujícím formátu
    
    x-coordinate y-coordinate class
    .
    .

a na standardní výstup se vypíše

    Solution found in <N> iterations.
    Needed <X> ms to finish.

### Příklady použití na přiložených datech

    ./kmeans -k 5 < data/5_200.txt
         - klasifikace do pěti tříd na dvou stech bodech, sekvenční běh
    ./kmeans -t -k 10 < data/10_1000.txt
         - klasifikace do deseti tříd na tisíci bodech, paralelní běh

### Vizualizace pomocí přiloženého skriptu v pythonu

>Pro vizualizace je nutné mít nainstalovanou knihovnu matplotlib pro python

    python ./data/visualize_data.py

![Example_visualization](output.png)

Skript se pokusí otevřít soubor output.txt vygenerovaný naším programem a uloží obrázek **output.png**

## Měření doby běhu

### Prostředí

Měření bylo prováděno na počítači s procesorem Intel Core i7-6700K 4.2 GHz, **8 vláken**, 16 GB RAM, Windows 10 na WSL2

### Způsob měření

Měření bylo prováděno s jedním milionem náhodných bodů, které jsme chtěli rozklasifikovat do 20 klastrů.

    ./kmeans [-t] -k 20 < data/20_1000000.txt

Vyhledem k tomu, že každý běh algroitmu může dokonvergovat v růzém počtu iterací, v tabulce měření je vždy uveden počet iterací a průměrný čas pěti běhů. Pro měření byla inicializace vždy stejná pro daný počet iterací. Byl měřen pouze běh samotného algoritmu. Načítání a ukládání a jiné operace nebyly měřeny.

|       -       | 43 iterací | 87 iterací | 19 iterací | 131 iterací |
|:-------------:|------------|------------|------------|-------------|
| Sekvenční běh | 4 468 ms   | 9 241 ms   | 1 941 ms   | 13 467 ms   |
| Paralelní běh | 3 418 ms   | 7 077 ms   | 1 560 ms   | 10 437 ms   |