#include "kmeans.hpp"

#include <algorithm>
#include <cmath>
#include <limits>
#include <mutex>
#include <random>
#include <thread>
#include <utility>

#define MAX_ITERATIONS 1000

double get_random_double(int from, int to)
{
    static std::mt19937 mt{std::random_device{}()};
    static std::uniform_real_distribution<> dist(from, to - 0.01);
    return dist(mt);
}

void findNearestMean(Point* p, MeansArray_t& means, int idx)
{
    size_t n_means = means.size();
    float x = p->getX();
    float y = p->getY();
    float min = std::numeric_limits<float>::max();
    unsigned int min_i = 0;
    float d;
    for(size_t j = 0; j < n_means; j++)
    {
        d = pow((x - means[j].get()->getX()), 2) + pow((y - means[j].get()->getY()), 2);
        if(d < min)
        {
            min = d;
            min_i = j;
        }
    }
    means[min_i].get()->addIndex(idx);
}

void findNearestMeanParallel(Point* p, MeansArray_t& means, int idx, std::vector<int>* data)
{
    size_t n_means = means.size();
    float x = p->getX();
    float y = p->getY();
    float min = std::numeric_limits<float>::max();
    unsigned int min_i = 0;
    float d;
    for(size_t j = 0; j < n_means; j++)
    {
        d = pow((x - means[j].get()->getX()), 2) + pow((y - means[j].get()->getY()), 2);
        if(d < min)
        {
            min = d;
            min_i = j;
        }
    }
    data[min_i].push_back(idx);
}

int kMeans(const Settings_s& s, PointsArray_t& points, MeansArray_t& means)
{
    size_t n_points = points.size();
    bool changed = false;
    bool flag = false;
    const unsigned int n_cpus = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    int n_iterations = 0;

    // lambda function called by each thread
    auto fnc = [&](int tid, int cpus, std::vector<int>* data)
    {
        for(size_t i = tid; i < n_points; i += cpus)
        {
            findNearestMeanParallel(points[i].get(), means, i, data);
        }
    };

    initialize(s.K, points, means);
    do
    {
        n_iterations++;
        for(size_t j = 0; j < means.size(); j++)
        {
            means[j].get()->vectorAssign();
        }

        // PARALLEL VERSION
        if(s.threaded && n_cpus)
        {
            std::vector<int>** threads_k_members = new std::vector<int>*[n_cpus];
            for(size_t i = 0; i < n_cpus; i++)
            {
                threads_k_members[i] = new std::vector<int>[s.K];
                threads.emplace_back(fnc, i, n_cpus, threads_k_members[i]);
            }
            for(auto& t : threads)
                t.join();
            threads.clear();

            // put together data from all threads
            for(size_t i = 0; i < n_cpus; i++)
            {
                for(size_t j = 0; j < means.size(); j++)
                {
                    means[j].get()->concatVectors(threads_k_members[i][j]);
                }
            }

            // freeing tmp arrays for threads
            for(size_t i = 0; i < n_cpus; i++)
            {
                delete[] threads_k_members[i];
            }
            delete[] threads_k_members;
        }
        // SEQUENTIAL VERSION
        else
        {
            for(size_t i = 0; i < n_points; i++)
            {
                findNearestMean(points[i].get(), means, i);
            }
        }

        // checking if clusters are nonempty
        flag = false;
        for(size_t j = 0; j < means.size(); j++)
        {
            // if so, reinitialize
            if(means[j].get()->checkEmpty())
            {
                if(n_iterations >= MAX_ITERATIONS)
                    break;
                initialize(s.K, points, means);
                flag = true;
                break;
            }
        }

        if(flag)
        {
            changed = true;
            continue;
        }

        //update means and compare if clusters changed
        changed = false;
        for(size_t j = 0; j < means.size(); j++)
        {
            means[j].get()->updateMean(points);
            if(means[j].get()->vectorChanged())
                changed = true;
        }

    } while(changed && n_iterations < MAX_ITERATIONS);

    std::cout << "Solution found in " << n_iterations << " iterations.\n";

    return 0;
}

void initialize(const int k, PointsArray_t& points, MeansArray_t& means)
{
    int n_points = points.size();
    means.clear();
    std::vector<int> indices;
    for(int i = 0; i < k; i++)
    {
        int idx;
        do
        {
            idx = (int)get_random_double(0, n_points);
        } while(std::end(indices) != std::find(std::begin(indices), std::end(indices), idx));

        indices.push_back(idx);

        means.push_back(std::make_unique<Mean>(points[idx].get()->getX(), points[idx].get()->getY(), idx));
    }
}