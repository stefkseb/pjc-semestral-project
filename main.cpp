#include "main.hpp"

#include <string.h>

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "handle_data.hpp"
#include "kmeans.hpp"
#include "point.hpp"

#define MAX_CLASSES 20

int parseAgruments(int& argc, char** argv, Settings_s& settings)
{
    for(int i = 1; i < argc; i++)
    {
        if(!strcmp(argv[i], "--help"))
        {
            std::cout << "This is a tiny utility for computing specified number of clusters for given "
                      << "set of points in 2D (20 at most). Result will be outputted into a file called "
                      << "kmeans_result.txt and will be in a format x-coordinate y-coordinate class [1..k].\n\n"
                      << "\t--help\t\tprints this menu\n"
                      << "\t-k [num]\tnumber of desired clusters, 2 if not specified\n"
                      << "\t-t\t\tmultithreaded computation will be enabled\n";

            return 1;
        }
        else if(!strcmp(argv[i], "-k"))
        {
            if(++i >= argc)
            {
                std::cout << "Number of clusters not specified!\n";
                return 1;
            }
            std::string num_classes_str(argv[i]);
            int num_classes;
            try
            {
                num_classes = std::stoi(num_classes_str);
            }
            catch(...)
            {
                std::cout << "Must be an integer and in range!\n";
                return 1;
            }
            if(num_classes < 2 || num_classes > MAX_CLASSES)
            {
                std::cout << "Must be an integer and in range!\n";
                return 1;
            }
            else
            {
                settings.K = num_classes;
            }
        }
        else if(!strcmp(argv[i], "-t"))
        {
            settings.threaded = true;
        }
        else
        {
            std::cout << "Invalid arguments\n";
            return 1;
        }
    }

    return 0;
}

int main(int argc, char* argv[])
{
    Settings_s settings{};
    std::vector<std::unique_ptr<Point>> points;
    std::vector<std::unique_ptr<Mean>> means;

    if(parseAgruments(argc, argv, settings))
        return 0;

    int points_loaded = loadPoints(points);
    if(points_loaded == 0)
    {
        std::cout << "No points loaded!\n";
        return 100;
    }
    else if(points_loaded < settings.K)
    {
        std::cout << "There is not enought points for specified number of clusters!\n";
        return 101;
    }

    // start the clock
    auto start = std::chrono::high_resolution_clock::now();

    kMeans(settings, points, means);

    // stop the clock
    auto end = std::chrono::high_resolution_clock::now();

    if(saveToFile(means, points))
    {
        std::cout << "Error while opening file!\n";
    }

    std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";

    return 0;
}