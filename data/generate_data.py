import random
import sys
import matplotlib.pyplot as plt

SIZE = 10000

if len(sys.argv) != 4:
    print("prvni argument je pocet trid (2-20), druhy argument je celkovy pocet bodu, treti je rozptyl")
    exit()

try:
    k = int(sys.argv[1])
    n = int(sys.argv[2])
    sigma = float(sys.argv[3])
except ValueError:
    print("argumenty mohou byt pouze cela cisla")

if k < 2 or k > 20:
    exit("pocet trid mimo rozsah")

#generate centers
centers = []
for i in range(k):
    centers.append((random.randrange(-SIZE,SIZE)/10,random.randrange(-SIZE,SIZE)/10))
    
points = []
for i in range(n):
    idx = random.randrange(0,k)
    points.append((round(random.gauss(centers[idx][0],sigma),2),round(random.gauss(centers[idx][1],sigma),2)))
    plt.plot(points[i][0], points[i][1], "kx")

plt.savefig('data/points.png')

file = open("data/points.txt", "w", encoding="utf-8")

for p in points:
    file.write(str(p[0]))
    file.write(" ")
    file.write(str(p[1]))
    file.write("\n")
