import matplotlib.pyplot as plt

PLOT_STYLES=("ro", "go", "bo", "co", "mo", "yo", "ko", "r|", "rx", "gx", "bx", "cx", "mx", "yx", "kx", "g|", "r*", "g*", "b*", "c*")

d = open("output.txt", "r")

data = []
for l in d.readlines():
    l = l.strip()
    data.append(l.split(" "))
print(len(data))

for x,y,c in data:
    plt.plot(float(x), float(y), PLOT_STYLES[int(c)], scalex=1, scaley=1)

plt.savefig('output.png')